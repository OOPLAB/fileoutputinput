/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.fileinputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author User
 */
public class Computer  implements Serializable{

    private int hand;
    private int playerHand;
    private int win, draw, lose;
    private  int status ;

    public Computer() {

    }

    private int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int playerHand) {
        this.playerHand = playerHand;
        this.hand = choob();
        if (this.playerHand == this.hand) {
            status=0;
            draw++;
            return 0;
        }
        if (this.playerHand == 0 && this.playerHand == 1) {
             status = 1;
            win++;
            return 1;
        }
        if (this.playerHand == 1 && this.playerHand == 2) {
            status=1;
            win++;
            return 1;
        }
        if (this.playerHand == 2 && this.playerHand == 0) {
            status=1;
            win++;
            return 1;
        }
        status=-1;
        lose++;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }

    public int getStatus() {
        return status;
    }

}
